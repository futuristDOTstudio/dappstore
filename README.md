# d-App Store - unlock new dimensions of applications.

![d-App Store grid](https://gitlab.com/futuristDOTstudio/dappstore/-/raw/master/images/d-App_grid.png "a title")


# contents
- paradigm (link)
- [foundations](https://gitlab.com/futuristDOTstudio/dappstore/-/blob/master/README.md#self-sovereign-foundations)
- [designed](https://gitlab.com/futuristDOTstudio/dappstore/-/blob/master/README.md#designed-for-delight-and-discovery)
- [contribute](https://gitlab.com/futuristDOTstudio/dappstore/-/blob/master/README.md#how-to-contribute) 

# a new paradigm
for over a decade, crypto has proved to be a safe and trusted place to create and store value. but crypto is more than just a blockchain database — it’s a truly transformative technology focused on bridging accessibility to opportunities through awesome experiences. and a big part of those experiences is ensuring that the apps we offer are held to the highest standards for privacy, security, and content.
 
because we are in a position to offer decentralised application experiences to hundrends to thousands if not million to billions -  we want you to feel awesome and assured about using every single one of them.


**the web you love, now with apps you can trust.**

*the web you love,
now not with you as the product.*

*the web you love,
now with apps that pay you.*

*the web you love,
now with a fair app ecosystem.*

*the web you love,
now with truly universal apps.*


better security, better trust, better economics.
superior user experience with monetary value exchange
built on the universal platform that is the web.

*say bye to proprietary, say hello to open source*

*say bye to exclusivity, say hello to inclusivity*



![Alt text](http://d-app.store/messaging/images/avatar-m.png "a title")

# self-sovereign foundations

C°iD -

crypto unlocks a new paradigm in online identity.

this enables a from the ground up rethinking of how applications operate.
from person to person value creation and exchange - to the user experience and interface - to the infrastructure that completes it.

ML - curated experience, dynamic discovery, L10N accessible from start, flag filtering, context aware composable experiences, context is the app, 

privacy for every person, security overkill for every context/app. At every level. browser, network, server. scanned source code

you choose what data to share - and with whom, and for how long.

[learn more here!](https://gitlab.com/futuristDOTstudio/dappstore/-/tree/master/CoiD)

# designed for delight and discovery.

starting with **Cºl⍺b**, where ideas copulate and reproduce, are fruitful and multiply.
[learn more here!](https://gitlab.com/futuristDOTstudio/dappstore/-/tree/master/Colab)

low to no code

server-side loading - local and remote (distributed)

GUI DLS IDE

graphical user interface, domain specific language, intergrated design environment.

network and storage optimised - say bye to "not enough storage"

# how to contribute

broken down by level/capacity - of time available to skillset.

0 - delegate

1 - user experience tester

2 - content creation/curation (caaast)

3 - ui,ux,code, app/infra




